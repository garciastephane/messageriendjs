package fr.afpa.test.services;



import java.util.List;
import java.util.Map;

import org.junit.Test;

import fr.afpa.entites.Personne;
import fr.afpa.services.ServiceVisualisation;
import junit.framework.TestCase;

public class TestServiceVisualisation extends TestCase {

	@Test
	public void testChaineNonVide() {
		String resultat = "";
		ServiceVisualisation sv = new ServiceVisualisation();
		resultat = sv.afficher();
		assertFalse("Initialisation, Table profil vide", "".equals(resultat));
	}
	
	@Test
	public void testRecupListDest() {
		ServiceVisualisation sv = new ServiceVisualisation();
		Map<Integer, Personne> list = sv.RecupListDest("admin, drBuilding");
		assertNotNull(list);
		Map<Integer, Personne> list2 = sv.RecupListDest("admin, drBuilding,aaa");
		assertNull(list2);
	}
}
