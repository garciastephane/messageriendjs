package fr.afpa.test.controles;

import org.junit.Test;

import fr.afpa.controles.ControleGeneral;
import junit.framework.TestCase;

public class TestControleGeneral extends TestCase {

	@Test
	public void testControleNomPrenom() {
		assertFalse("Probleme controle nom", ControleGeneral.controleNomPrenom("24truc32"));
		assertFalse("Probleme controle nom", ControleGeneral.controleNomPrenom(null));
		assertTrue("Probleme controle nom", ControleGeneral.controleNomPrenom("Bruce"));
	}

	@Test
	public void testControleDate() {
		assertFalse("Probleme controle date", ControleGeneral.controleDateDeNaissance("1235452"));
		assertFalse("Probleme controle date", ControleGeneral.controleDateDeNaissance(null));
		assertFalse("Probleme controle date", ControleGeneral.controleDateDeNaissance("1994-12-25"));
		assertTrue("Probleme controle date", ControleGeneral.controleDateDeNaissance("25/12/1994"));
		
	}
	
	@Test
	public void testControleRole() {
		assertTrue("Probleme controle role", ControleGeneral.controleRole("stagiaire"));
		assertTrue("Probleme controle role", ControleGeneral.controleRole("FORMATEUR"));
		assertFalse("Probleme controle role", ControleGeneral.controleRole("manager"));
		assertFalse("Probleme controle role", ControleGeneral.controleRole(null));
	}
	
}
