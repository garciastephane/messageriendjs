package fr.afpa.dto;

import fr.afpa.entites.Administrateur;
import fr.afpa.entites.Personne;
import fr.afpa.entites.Utilisateur;
import fr.afpa.entitespersistees.ProfilBDD;
import fr.afpa.services.ServiceGeneral;

public class DTOGeneral {

	/**
	 * Permet de transformer une instance de RoleBDD en instance de RolePersonne
	 * 
	 * @param role : le role a transformer
	 * @return le role en instance de RolePersonne
	 */


	/**
	 * Permet de transformer une instance de ProfilBDD en instance d'Administrateur
	 * ou d'Utilisateur
	 * 
	 * @param profilBDD : le profil a transformer
	 * @return une instance qui herite d'une classe fille de la classe Personne
	 */
	public static Personne ProfilBDDToPersonne(ProfilBDD profilBDD) {
		Personne personne;
		if ("Administrateur".equals(profilBDD.getTypeProfil().getLibelle())) {
			personne = new Administrateur();
		} else {
			personne = new Utilisateur();
		}
		personne.setNom(profilBDD.getNom());
		personne.setPrenom(profilBDD.getPrenom());
		personne.setDateNaissance(ServiceGeneral.conversionDate(profilBDD.getDateNaissance()));
		personne.setEmail(profilBDD.getMail());
		personne.setAdresse(profilBDD.getAdresse());
		personne.setActif(profilBDD.isActif());
		return personne;
	}

	/**
	 * Permet de transformer une instance de ProfilBDD en instance d'Administrateur
	 * ou d'Utilisateur
	 * 
	 * @param profilBDD : le profil a transformer
	 * @return une instance qui herite d'une classe fille de la classe Personne
	 */
	public static ProfilBDD personneToProfilBDD(Personne personne) {
		ProfilBDD profil = new ProfilBDD();
		profil.setAdresse(personne.getAdresse());
		profil.setDateNaissance(ServiceGeneral.conversionDate(personne.getDateNaissance()));
		profil.setMail(personne.getEmail());
		profil.setNom(personne.getNom());
		profil.setPrenom(personne.getPrenom());
		return profil;
	}

}
