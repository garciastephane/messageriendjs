package fr.afpa.dto;

import java.util.List;

import fr.afpa.dao.DAOCreation;
import fr.afpa.dao.DAOLecture;
import fr.afpa.entites.Message;
import fr.afpa.entitespersistees.MessageBDD;
public class DTOMessage {

	

	public boolean EnvoiMessageDto(String exp , String dest , String obj, String corps) {
		DAOCreation daoc = new DAOCreation();
		DAOLecture daol = new DAOLecture();
		MessageBDD mbdd = new MessageBDD();
		mbdd.setExpediteur(daol.listeProfils(exp).get(0));
		mbdd.setDestinataires(daol.listeProfils(dest));
		if(mbdd.getDestinataires() != null) {
			mbdd.setObjet(obj);
			mbdd.setCorps(corps);
			daoc.envoiMessageDao(mbdd);
			return true;
		}
		return false;
	}

	public List<Message> voirMessages(String i) {
		return new DAOLecture().voirMessages(i);
	}

}
