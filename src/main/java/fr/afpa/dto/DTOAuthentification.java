package fr.afpa.dto;

import fr.afpa.dao.DAOAuthentification;
import fr.afpa.entites.Personne;

public class DTOAuthentification {

	public Personne recupPersonne(String login) {
		return new DAOAuthentification().recupPersonne(login);
	}

	public String recupIdPersonne(String login) {
		return new DAOAuthentification().recupIdPersonne(login);

	}

}
