package fr.afpa.entites;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter

public class Message {
	
	private int idMes;
	private String objet;
	private String corps;
	private Personne expediteur;
	private Map<Integer, Personne> destinataires;

	/**
		 * 
		 */
		public Message() {
			super();
		}

	/**
		 * @param idMes
		 * @param objet
		 * @param corps
		 * @param expediteur
		 * @param destinataires
		 */
		public Message(int idMes, String objet, String corps, Personne expediteur, Map<Integer, Personne> destinataires) {
			super();
			this.idMes = idMes;
			this.objet = objet;
			this.corps = corps;
			this.expediteur = expediteur;
			this.destinataires = destinataires;
		}

}
