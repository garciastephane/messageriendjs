package fr.afpa.services;

import java.time.LocalDate;

import fr.afpa.dto.DTOMessage;
import fr.afpa.dto.DTOUtilisateur;
import fr.afpa.entites.Administrateur;
import fr.afpa.entites.Personne;

import fr.afpa.entites.Utilisateur;

public class ServiceCreation {

	/**
	 * Permet de créer une personne
	 * 
	 * @param nom           le nom de la personne
	 * @param prenom        le prenom de la personne
	 * @param dateNaissance la date de naissance de la personne
	 * @param mail          le mail de la personne
	 * @param adresse       l'adresse de la personne
	 * @param role          le role de la personne
	 * @param login         le login de la personne
	 * @param mdp           le mot de passe de la personne
	 * @param admin         le type de personne (true = admin, false = utilisateur)
	 * @return une nouvelle personne
	 */
	public Personne creationPersonne(String nom, String prenom, LocalDate dateNaissance, String mail, String adresse,
			String login, String mdp, boolean admin) {
		if (mdp != null) {
			Personne p;
			DTOUtilisateur dtou = new DTOUtilisateur();
			if (admin) {
				p = new Administrateur(nom, prenom, dateNaissance, mail, adresse, true);
			} else {
				p = new Utilisateur(nom, prenom, dateNaissance, mail, adresse, true);
			}
			dtou.ajoutBDD(p, login, mdp);
			return p;
		} else
			return null;
	}


	public boolean EnvoiMessage(String exp, String dest, String obj, String corps) {
		DTOMessage dtom = new DTOMessage();
		if(dtom.EnvoiMessageDto(exp, dest, obj, corps)) {
			return true;
		}
		return false;
		

	}
}
