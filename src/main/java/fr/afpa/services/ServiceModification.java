package fr.afpa.services;

import fr.afpa.dto.DTOUtilisateur;
import fr.afpa.entites.Personne;

public class ServiceModification {

	public static final String CHOIX = "choix";

	/**
	 * Permet de supprimer un utilisateur en passant par le DTO
	 * 
	 * @param id l'id de l'utilisateur a supprimer
	 * @return true si l'utilisateur est supprimer, false si non
	 */
	public boolean supprimerUtilisateur(int id) {
		DTOUtilisateur dtou = new DTOUtilisateur();
		return dtou.suppressionBDD(id);
	}

	/**
	 * Permet de activer ou desactiver un utilisateur en passant par le DTO
	 * 
	 * @param id l'id de l'utilisateur a desactiver
	 * @param etat de l'activation ou non du compte
	 * @return true si l'utilisateur est desactiver, false si non
	 */
	public boolean activerDesactiverUtilisateur(int id) {
		DTOUtilisateur dtou = new DTOUtilisateur();
		return dtou.desactiverBDD(id);
	}
	

	/**
	 * Permet de modifier un utilisateur en passant par le DTO
	 * 
	 * @param user l'utilisateur a modifier
	 * @param id   l'id de l'utilisateur
	 * @param mdp  le nouveau mot de passe
	 * @return true si l'utilisateur est modifier, false si non
	 */
	public boolean modifierUtilisateur(Personne user, int id, String mdp) {
		DTOUtilisateur dtou = new DTOUtilisateur();
		return dtou.modifierBDD(user, id, mdp);
	}
}
