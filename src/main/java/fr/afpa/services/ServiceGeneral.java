package fr.afpa.services;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;


public class ServiceGeneral {

	private ServiceGeneral() {
	}
	
	/**
	 * Permet de convertir une date de type sql.Date en date de type LocalDate
	 * 
	 * @param date : la date a convertir
	 * @return la date convertie en LocalDate
	 */
	public static LocalDate conversionDate(Date date) {
		return date.toLocalDate();
	}

	/**
	 * Permet de convertir une date de type LocalDate en date de type sql.Date
	 * 
	 * @param date : la date a convertir
	 * @return la date convertie en sql.Date
	 */
	public static Date conversionDate(LocalDate date) {
		return Date.valueOf(date);
	}

	/**
	 * Permet de convertir une date de type String en date de type LocalDate
	 * 
	 * @param date la date a convertir
	 * @return la date convertie
	 */
	public static LocalDate conversionDate(String date) {
		LocalDate lDate = null;
		try {
			lDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
		} catch (DateTimeParseException e) {
			lDate = LocalDate.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
		}
		return lDate;
	}


}
