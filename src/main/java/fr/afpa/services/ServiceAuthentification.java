package fr.afpa.services;

import fr.afpa.dto.DTOAuthentification;
import fr.afpa.entites.Personne;

public class ServiceAuthentification {

	public Personne recupererPersonne(String login) {
		return new DTOAuthentification().recupPersonne(login);
	}
	
	public String recupereridPersonne(String login) {
		return new DTOAuthentification().recupIdPersonne(login);
	}

}
