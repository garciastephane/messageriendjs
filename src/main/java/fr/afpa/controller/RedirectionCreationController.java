package fr.afpa.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RedirectionCreationController {

	public RedirectionCreationController() {
		super();
	}

	@RequestMapping(value = "RC", method = RequestMethod.POST)
	public String redirectionCreation() {
			return "creationutilisateur";

	}

}
