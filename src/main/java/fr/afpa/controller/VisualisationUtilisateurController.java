package fr.afpa.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.afpa.entites.Personne;
import fr.afpa.services.ServiceVisualisation;

/**
 * Servlet implementation class ServletVisualisationUtilisateur
 */
public class VisualisationUtilisateurController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public VisualisationUtilisateurController() {
		super();
	}

	@RequestMapping(value = "VU", method = RequestMethod.POST)
	public String visualisation(@RequestParam String listePersonne) {
		RedirectAttributes r = new RedirectAttributesModelMap();
		ServiceVisualisation sv = new ServiceVisualisation();
		Map<Integer, Personne> listePersonnes = sv.listeTousPersonnes();
		r.addAttribute(listePersonne, listePersonnes);
		return "visualisationutilisateur";
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
