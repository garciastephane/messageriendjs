package fr.afpa.controller;

import java.sql.Date;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.afpa.entites.Personne;
import fr.afpa.entites.Utilisateur;
import fr.afpa.services.ServiceGeneral;
import fr.afpa.services.ServiceModification;

@Controller
public class ModificationUtilisateurController {

	public ModificationUtilisateurController() {
		super();
	}

	@RequestMapping(value = "MU", method = RequestMethod.POST)
	public String modification(@RequestParam String password, String modif, @RequestParam String password2, String nom,
			String prenom, String mail, String adresse, Date datenaissance, String id) {
		String req = modif;
		ServiceModification sm = new ServiceModification();
		switch (req) {
		case "valider":
			if (password.equals(password2)) {
				Personne user = new Utilisateur();
				user.setNom(nom);
				user.setPrenom(prenom);
				user.setEmail(mail);
				user.setAdresse(adresse);
				user.setDateNaissance(ServiceGeneral.conversionDate(datenaissance));
				sm.modifierUtilisateur(user, Integer.parseInt(id), password);
			}
			break;
		case "activer / desactiver":
			sm.activerDesactiverUtilisateur(Integer.parseInt(id));
			break;
		case "supprimer":
			sm.supprimerUtilisateur(Integer.parseInt(id));
			break;
		default:
			break;
		}
		return "index";
	}

}
