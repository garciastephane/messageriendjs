package fr.afpa.controller;

import java.time.LocalDate;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.afpa.controles.ControleCreationUtilisateur;
import fr.afpa.controles.ControleGeneral;
import fr.afpa.services.ServiceCreation;
import fr.afpa.services.ServiceGeneral;

@Controller
public class CreationUtilisateurController {

	public CreationUtilisateurController() {
		super();
	}

	@RequestMapping(value = "CU", method = RequestMethod.POST)
	public String creerUtilisateur(@RequestParam String prenom, @RequestParam String nom, @RequestParam String mail,
			@RequestParam String adresse, @RequestParam String datenaissance, @RequestParam String password,
			@RequestParam String password2, @RequestParam String login, @RequestParam String create) {
		RedirectAttributes r = new RedirectAttributesModelMap();
		ServiceCreation sc = new ServiceCreation();
		String nom1 = "";
		String prenom1 = "";
		String mail1;
		String adresse1;
		LocalDate dateNaissance1 = LocalDate.now();
		String login1 = "";
		String password1 = null;
		if (ControleGeneral.controleNomPrenom(nom)) {
			nom1 = nom;
		}
		if (ControleGeneral.controleNomPrenom(prenom)) {
			prenom1 = prenom;
		}
		mail1 = mail;
		adresse1 = adresse;

		if (ControleGeneral.controleDateDeNaissance(datenaissance)) {
			dateNaissance1 = ServiceGeneral.conversionDate(datenaissance);
		}
		if (password.equals(password2)
				&& ControleCreationUtilisateur.controleLogin(login)) {
			login1 = login;
			password1 = password;
		} else {
			r.addAttribute("nom", nom);
			r.addAttribute("prenom", prenom);
			r.addAttribute("mail", mail);
			r.addAttribute("adresse", adresse);
			r.addAttribute("datenaissance", datenaissance);
			r.addAttribute("login", login);
			if (ControleCreationUtilisateur.controleLogin(login))
				r.addAttribute("existe", false);
			else
				r.addAttribute("existe", true);
			return "creationutilisateur";
		}
		if ("user".equals(create)) {
			sc.creationPersonne(nom1, prenom1, dateNaissance1, mail1, adresse1, login1, password1, false);
		} else if ("admin".equals(create)) {
			sc.creationPersonne(nom1, prenom1, dateNaissance1, mail1, adresse1, login1, password1, true);
		}
		return "gestionuser";
	}

}
