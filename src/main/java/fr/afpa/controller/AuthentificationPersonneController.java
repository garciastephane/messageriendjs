package fr.afpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.afpa.controles.ControleAuthentificationUtilisateur;
import fr.afpa.entites.Administrateur;
import fr.afpa.entites.Personne;
import fr.afpa.services.ServiceAuthentification;

@Controller
public class AuthentificationPersonneController {
	public static final String AUTHENTIFICATION = "authentification";
	public static final String LOGIN = "login";
	
	public AuthentificationPersonneController() {
		super();
	}

	@RequestMapping(value = "SAP", method = RequestMethod.POST)
	public String authentification (@RequestParam String login, @RequestParam String password) {
		ControleAuthentificationUtilisateur cau = new ControleAuthentificationUtilisateur();
		if (cau.controlePersonneInscrite(login, password)) {
			Personne p = new ServiceAuthentification().recupererPersonne(login);
			RedirectAttributes r = new RedirectAttributesModelMap();
			r.addAttribute(AUTHENTIFICATION, p instanceof Administrateur);
			r.addAttribute(AUTHENTIFICATION, login);
			return "gestionuser";
		} else {
			return "index";
		}
		
	}

}
