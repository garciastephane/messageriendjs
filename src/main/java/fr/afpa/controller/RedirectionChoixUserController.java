package fr.afpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.afpa.services.ServiceModification;
import fr.afpa.services.ServiceVisualisation;

@Controller
public class RedirectionChoixUserController {

	public RedirectionChoixUserController() {
		super();
	}
	@RequestMapping(value = "RCU", method = RequestMethod.POST)
	public String redirectionCU() {
		RedirectAttributes r = new RedirectAttributesModelMap();
		r.addAttribute("choix", ServiceModification.CHOIX);
		r.addAttribute("alluser", new ServiceVisualisation().afficherUser());
		return "choixuser";
	}

}
