package fr.afpa.controller;

import java.io.IOException;

import javax.servlet.ServletException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import fr.afpa.services.ServiceCreation;

@Controller
public class EnvoiMessageController {

	
	public EnvoiMessageController() {
		super();
	}

	@RequestMapping(value = "EM", method = RequestMethod.POST)
	public String envoiMessage(@RequestParam String dest, @RequestParam String obj, @RequestParam String message, @RequestParam String login)
			throws ServletException, IOException {
		ServiceCreation sc = new ServiceCreation();
			sc.EnvoiMessage(login, dest, obj, message);
			return "gestionuser";

	}

}
