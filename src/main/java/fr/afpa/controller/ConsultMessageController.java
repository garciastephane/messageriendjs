package fr.afpa.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.afpa.services.ServiceAuthentification;
import fr.afpa.services.ServiceMessage;

@Controller
public class ConsultMessageController {

    public ConsultMessageController() {
        super();
    }

    @RequestMapping(value = "CM", method = RequestMethod.POST)
	public String consulterMessage(@RequestParam String log, HttpServletResponse response){
    	RedirectAttributes r = new RedirectAttributesModelMap();
		ServiceAuthentification sa = new ServiceAuthentification();
		String id = sa.recupereridPersonne(log);
		r.addAttribute("listeMessages", ServiceMessage.voirMessages(id));
		
		return "consultermessages";
		
	}

}
