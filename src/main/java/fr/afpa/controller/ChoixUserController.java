package fr.afpa.controller;

import java.time.format.DateTimeFormatter;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import fr.afpa.controles.ControleChoixUtilisateur;
import fr.afpa.dto.DTOUtilisateur;
import fr.afpa.entites.Personne;
import fr.afpa.entites.Utilisateur;
import fr.afpa.services.ServiceModification;
import fr.afpa.services.ServiceVisualisation;

@Controller
public class ChoixUserController{

	public ChoixUserController() {
		super();
	}
	
	@RequestMapping(value = "ChU", method = RequestMethod.POST)
	public String choixUser(@RequestParam String choix) {
		RedirectAttributes r = new RedirectAttributesModelMap();
		DTOUtilisateur dtou = new DTOUtilisateur();
		Map<Integer, Personne> listePersonnes = dtou.listePersonnes();
		if (ControleChoixUtilisateur.verificationChoix(choix)) {
			Personne personne = listePersonnes.get(Integer.parseInt(choix));
			if (personne instanceof Utilisateur) {
				r.addAttribute("personne", personne);
				r.addAttribute("id", Integer.parseInt(choix));
				r.addAttribute("datenaissance",
						personne.getDateNaissance().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
				return "modificationutilisateur";
			} else {
				r.addAttribute("choix", ServiceModification.CHOIX);
				r.addAttribute("alluser", new ServiceVisualisation().afficherUser());
				return "choixuser";
			}
		} else {
			r.addAttribute("choix", ServiceModification.CHOIX);
			r.addAttribute("alluser", new ServiceVisualisation().afficherUser());
			return "choixuser";
		}
	}

}
