package fr.afpa.controller;

import org.springframework.stereotype.Controller;

@Controller
public class DeconnectionController {

	public DeconnectionController() {
		super();
	}

	public String deconnexion() {
		return "index";
	}

}
