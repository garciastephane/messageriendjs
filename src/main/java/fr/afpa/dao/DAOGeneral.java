package fr.afpa.dao;

import fr.afpa.entites.Administrateur;
import fr.afpa.entites.Message;
import fr.afpa.entites.Personne;
import fr.afpa.entites.Utilisateur;
import fr.afpa.entitespersistees.MessageBDD;
import fr.afpa.entitespersistees.ProfilBDD;

public class DAOGeneral {

	public Message convertMessageBDDToMessage(MessageBDD mes) {
		Message m = new Message();
		m.setCorps(mes.getCorps());
		m.setExpediteur(convertProfilBDDToPersonne(mes.getExpediteur(), mes.getExpediteur().getLoginMdp().getLogin()));
		m.setObjet(mes.getObjet());
		return m;
	}

	private Personne convertProfilBDDToPersonne(ProfilBDD expediteur, String log) {
		Personne p;
		if("Administrateur".equals(expediteur.getTypeProfil().getLibelle()))
			p = new Administrateur();
		else
			p = new Utilisateur();
		p.setNom(log);
		return p;
	}

}
