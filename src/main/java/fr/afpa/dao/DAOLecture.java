package fr.afpa.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.entites.Message;
import fr.afpa.entitespersistees.LogBDD;
import fr.afpa.entitespersistees.MessageBDD;
import fr.afpa.entitespersistees.ProfilBDD;
import fr.afpa.utils.HibernateUtils;

public class DAOLecture {

	/**
	 * Permet de retourner la liste de tous les profils
	 * 
	 * @return la liste de tous les profils dans la base de donnees
	 */
	public List<ProfilBDD> listeTousProfils() {
		Session session = HibernateUtils.getSession();
		Query query = session.createQuery("from profil");
		List<ProfilBDD> listeProfils = (ArrayList<ProfilBDD>) query.list();
		session.close();
		return listeProfils;
	}

	/**
	 * Permet de Lire tout les logs
	 * 
	 * @return la liste des logs
	 */
	public List<LogBDD> listeTousLogs() {
		Session session = HibernateUtils.getSession();
		Query query = session.createQuery("from login");
		List<LogBDD> listeLogs = (ArrayList<LogBDD>) query.list();
		session.close();
		return listeLogs;
	}

	/**
	 * Permet de lire tout les utilisateurs
	 * 
	 * @return la liste des utilisateurs
	 */
	public List<ProfilBDD> listeTousUser() {
		Session session = HibernateUtils.getSession();
		Query query = session.createQuery("from profil where id_type_profil=2");
		List<ProfilBDD> listeLogs = (List<ProfilBDD>) query.list();
		session.close();
		return listeLogs;
	}

	/**
	 * Permet de retourner une liste qui contient le profil recherche via son login
	 * et son mot de passe
	 * 
	 * @return une liste contenant le profil rechercher s'il est trouve et une liste
	 *         vide sinon
	 */
	public List<LogBDD> authentification(String login, String mdp) {
		Session session = HibernateUtils.getSession();
		Query query = session.createQuery("from login where login.login='" + login + "' and motdepasse='" + mdp + "'");
		List<LogBDD> listeLMDP = (ArrayList<LogBDD>) query.list();
		session.close();
		return listeLMDP;
	}

	public List<ProfilBDD> listeProfils(String parameter) {
		String[] logins = parameter.split(" *, *");
		for (int i = 1; i < logins.length; i++) {
			logins[0] += "','" + logins[i];
		}
		Session session = HibernateUtils.getSession();
		Query query = session.createQuery("from login  where login.login in ('" + logins[0] + "')");
		List<LogBDD> listelog = (ArrayList<LogBDD>) query.list();
		List<ProfilBDD> listeProfils = new ArrayList<ProfilBDD>();
		if (logins.length == listelog.size()) {
			for (LogBDD log : listelog) {
				listeProfils.add(log.getProfil());
			}
			session.close();
			return listeProfils;
		}
		return null;
	}

	public List<Message> voirMessages(String i) {
		Session session = HibernateUtils.getSession();
		Query query = session.createQuery("from message");
		Query query2 = session.createQuery("from profil where id_profil = "+i+"");
		ProfilBDD p = (ProfilBDD) query2.list().get(0);
		List<MessageBDD> listelog = (ArrayList<MessageBDD>) query.list();
		List<Message> listeM = new ArrayList<Message>();
		for (MessageBDD messageBDD : listelog) {
			if(messageBDD.getDestinataires().contains(p)) {
				listeM.add(new DAOGeneral().convertMessageBDDToMessage(messageBDD));
			}
		}
		System.out.println(listeM);
		session.close();
		return listeM;
	}

}
