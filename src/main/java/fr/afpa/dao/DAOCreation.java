package fr.afpa.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.entites.Administrateur;
import fr.afpa.entites.Personne;
import fr.afpa.entitespersistees.LogBDD;
import fr.afpa.entitespersistees.MessageBDD;
import fr.afpa.entitespersistees.ProfilBDD;
import fr.afpa.entitespersistees.TypeProfilBDD;
import fr.afpa.utils.HibernateUtils;

public class DAOCreation {

	/**
	 * 
	 */
	public DAOCreation() {
		super();
	}

	/**
	 * Permet d'enregistrer un utilisateur dans la base de donnee
	 * 
	 * @param profil   le profil de l'utilisateur
	 * @param log      les logs de l'utilisateur
	 * @param personne l'utilisateur
	 * @return true si l'enregistrement est effectuer, false si non
	 */
	public boolean enregistrerUtilisateur(ProfilBDD profil, LogBDD log, Personne personne) {
		Session session = HibernateUtils.getSession();
		TypeProfilBDD typeProfil;
		
		// obtention du type de profil
		if (personne instanceof Administrateur) {
			typeProfil = session.get(TypeProfilBDD.class, 1);
		} else {
			typeProfil = session.get(TypeProfilBDD.class, 2);
		}
		profil.setActif(false);
		profil.setTypeProfil(typeProfil);
		Transaction tx = session.beginTransaction();
		session.save(profil);
		session.save(log);
		tx.commit();
		session.close();
		return false;
	}

	public void envoiMessageDao(MessageBDD mbdd) {
		Session session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		session.save(mbdd);
		tx.commit();
		session.close();
	}

}
