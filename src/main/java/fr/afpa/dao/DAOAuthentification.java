package fr.afpa.dao;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.entites.Administrateur;
import fr.afpa.entites.Personne;
import fr.afpa.entites.Utilisateur;
import fr.afpa.entitespersistees.LogBDD;
import fr.afpa.entitespersistees.ProfilBDD;
import fr.afpa.utils.HibernateUtils;

public class DAOAuthentification {

	public Personne recupPersonne(String login) {
		Session session = HibernateUtils.getSession();
		Query query = session.createQuery("from login where login = '"+login+"'");
		Query query2 = session.createQuery("from profil where id_profil = '"+((LogBDD) query.list().get(0)).getProfil().getId_profil()+"'");
		ProfilBDD p = (ProfilBDD) query2.list().get(0);
		Personne per;
		if ("Administrateur".equals(p.getTypeProfil().getLibelle())){
			per = new Administrateur();
		}
		else {
			per = new Utilisateur();
		}
		session.close();
		return per;
	}
	
	public String recupIdPersonne(String login) {
		Session session = HibernateUtils.getSession();
		Query query = session.createQuery("from login where login = '"+login+"'");
		Query query2 = session.createQuery("from profil where id_profil = '"+((LogBDD) query.list().get(0)).getProfil().getId_profil()+"'");
		ProfilBDD p = (ProfilBDD) query2.list().get(0);
		String id  = ""+p.getId_profil();
		return id;
	}
}
