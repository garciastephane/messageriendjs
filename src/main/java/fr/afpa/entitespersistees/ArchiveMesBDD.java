package fr.afpa.entitespersistees;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity(name = "archiveMessage")
public class ArchiveMesBDD {

	@SequenceGenerator(name = "ARCHIVEMES_SEQ", initialValue = 1, allocationSize = 1)
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ARCHIVEMES_SEQ")
	private int id_archiveMes;

	@Column
	private String objet;
	@Column
	private String corps;

	@Column
	private String expediteur;

	@Column
	private String destinataires;

	/**
	 * 
	 */
	public ArchiveMesBDD() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param id_archiveMes
	 * @param objet
	 * @param corps
	 * @param expediteur
	 * @param destinataires
	 */
	public ArchiveMesBDD(int id_archiveMes, String objet, String corps, String expediteur, String destinataires) {
		super();
		this.id_archiveMes = id_archiveMes;
		this.objet = objet;
		this.corps = corps;
		this.expediteur = expediteur;
		this.destinataires = destinataires;
	}
	
	

}
