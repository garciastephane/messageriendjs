package fr.afpa.entitespersistees;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@Entity(name = "login")
public class LogBDD {

	@Id
	@OnDelete(action = OnDeleteAction.CASCADE)
	private String login;

	@Column
	private String motdepasse;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_profil")
	private ProfilBDD profil;

	public LogBDD() {
		super();
	}

	public LogBDD(String login, String mdp) {
		super();
		this.login = login;
		this.motdepasse = mdp;
	}

}
