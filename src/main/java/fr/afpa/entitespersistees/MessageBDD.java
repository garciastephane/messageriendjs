package fr.afpa.entitespersistees;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

@Entity(name = "message")
public class MessageBDD {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MESSAGE_SEQ")
	@OnDelete(action = OnDeleteAction.CASCADE)
	private int idMes;
	@Column
	private String objet;
	@Column
	private String corps;
	
	@ManyToOne
	@JoinColumn(name = "id_profil")
	private ProfilBDD expediteur;
	
	@ManyToMany
	@JoinColumn(name = "id_profil")
	private List<ProfilBDD> destinataires;

	
	
	/**
	 * @param idMes
	 * @param objet
	 * @param corps
	 * @param expediteur
	 * @param destinataires
	 */
	public MessageBDD(int idMes, String objet, String corps, ProfilBDD expediteur, List<ProfilBDD> destinataires) {
		super();
		this.idMes = idMes;
		this.objet = objet;
		this.corps = corps;
		this.expediteur = expediteur;
		this.destinataires = destinataires;
	}



	/**
	 * 
	 */
	public MessageBDD() {
		super();
	}


}
