<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/styles.css">

<title>Visualisation Utilisateur</title>
</head>
<body>
	<div class="container" >

		<div class="bg-white rounded ">


			<c:choose>
				<c:when test="${ empty sessionScope.authentification }">
					<c:redirect url="r" />
				</c:when>
			</c:choose>

			<div class="container">
				<br>
				<br>
				<div class="text-sm-center">
					<h2>
					<strong>LISTE DE TOUTES LES PERSONNES</strong>
					</h2>
					<br>
					<br>
					<div class=col-md-12>
						<table class="table">

							<thead>
								<tr class="table table-dark ">
									<td>Nom</td>
									<td>Prenom</td>
									<td>Date de naissance</td>
									<td>Actif</td>
									<td>Mail</td>
									<td>Adresse</td>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="couplePersonne" items="${listePersonnes}">

									

										<tr>
											<td><c:out value="${couplePersonne ['value'] ['nom'] } " /></td>
											<td><c:out
													value="${couplePersonne ['value'] ['prenom'] } " /></td>
											<td><c:out
													value="${couplePersonne ['value'] ['dateNaissance'] } " /></td>
											<td><c:out
													value="${couplePersonne ['value'] ['actif'] } " /></td>
											<td><c:out
													value="${couplePersonne ['value'] ['email'] } " /></td>
											<td><c:out
													value="${couplePersonne ['value'] ['adresse'] } " /></td>
										</tr>
								</c:forEach>

							</tbody>
						</table>
						<br />
						<div>
							<form action="Retour" method="post">
								<button class="btn btn-dark">Retour</button>
							</form>
						</div>
					</div>
					<br> <br>
</body>
</html>