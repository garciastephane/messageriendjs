<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Creer un utilisateur</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/styles.css">

</head>



<body>
	<div class="container" id="containerMessage">

		<div class="bg-white rounded ">
			<div class="text-sm-center">
				<br /> <br />
				<h2>
					<strong>CREATION D'UN UTILISATEUR</strong>
				</h2>
			</div>
			<br /> <br />
			<div class="container">
				<form class="needs-validation" novalidate action="SCU" method="post">
					<div class="text-sm-center">
						<label>Nom</label> <input type="text" class="form-control"
							name="nom" value="">
					</div>
					<br>
					<div class="text-sm-center">
						<label>Prenom</label> <input type="text" class="form-control"
							name="prenom"
							value=''>
					</div>
					<br>
					<div class="text-sm-center">
						<label>Mail</label>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">@</span>
							</div>
							<input type="text" class="form-control" name="mail"
								value=''>


						</div>
						<br>

					</div>


					<div class="text-sm-center">
						<label>Adresse</label> <input type="text" class="form-control"
							name="adresse"
							value=''>

					</div>
					<br>

					<div class="text-sm-center">
						<label>Date de Naissance</label> <input type="date"
							class="form-control" name="datenaissance"
							value=''>

					</div>
					<br>
					<div class="text-sm-center">
						<label>Login</label> <input type="text" class="form-control"
							name="login"
							value=''> <label>
							<c:if test="${ existe eq true }">
								<c:out value="le login est incorrect ou existe deja"></c:out>
							</c:if>
						</label>
					</div>

					<div class="text-sm-center">
						<label>MotDePasse</label> <input type="password"
							class="form-control" name="password" value="">

					</div>
					<br>
					<div class="text-sm-center">
						<label>Valider Mot De Passe</label> <input type="password"
							class="form-control" name="password2" value="">

					</div>
					<br> <br>
					<div class="col text-center">
						<button class="btn btn-primary " type="submit" name="create"
							value="user">Creer un Utilisateur</button>
						<br> <br>
					</div>
				</form>
				<form action="r" method="post">
					<button class="btn btn-dark" type="submit">Retour</button>
					<br> <br>
				</form>
			</div>
		</div>
	</div>
</body>
</html>