<!doctype html>
<html>
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/styles.css">

</head>

<body>
	<div class="container" id="containerMessage">

		<div class="bg-white rounded ">
			<div class="container">
				<div class="text-sm-center">
					<br> <br>
					<h2>
						<strong>AUTHENTIFICATION</strong>
					</h2>
				</div>
				<br /> <br /> <br />
				<div class="container">
					<form action="SAP" method="post">
						<div class="form-group row">
							<label for="login" class="col-sm-2 col-form-label">Login</label>
							<div class="col-sm-10">
								<input type="text" class="form-control"
									placeholder="Entrez votre Login" id="login" name="login">
							</div>
						</div>
						<div class="form-group row">
							<label for="password" class="col-sm-2 col-form-label">Password</label>
							<div class="col-sm-10">
								<input type="password" class="form-control"
									placeholder="Entrez votre Password" id="password"
									name="password">
							</div>
						</div>


						<div class="text-sm-center">
							<button type="submit" class="btn btn-dark">Connexion</button>
							<br> <br>
						</div>
					</form>
					<form action="SRC" method="get">
						<button type="submit" class="btn btn-primary">Cr�er un nouveau compte</button>
						<br>
						<br>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
