<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<title>Gestion des Utilisateurs</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/styles.css">

</head>
<body>
	<div class="container" id="containerMessage">

		<div class="bg-white rounded ">

			<div class="text-sm-center">
				<br> <br>
				<h2>
					<strong>GESTION</strong>
				</h2>
				<br /> <br />
			</div>

			<div class="container">
				<div class="card text-center">

					<div class="card-body ">
						<form action="SCM" method="post">
							<a href="SCM">Consulter ses messages</a>
						</form>
					</div>
				</div>
				<div class="card text-center">

					<div class="card-body ">

						<a href="SRE">Envoi message</a>
					</div>
				</div>


				<c:if
					test="${ authentification.getClass().getSimpleName() eq 'Administrateur' }">
					<div class="card text-center">
						<div class="card-body">
							<form action="SVU" method="post">
								<a href="SVU">Liste des Utilisateurs</a>
							</form>

						</div>
					</div>

					<div class="card text-center">

						<div class="card-body">
							<form action="SRCU">
								<a href="SRCU">Modifier un Utilisateur</a>
							</form>

						</div>
					</div>
				</c:if>
			</div>
			<br /> <br />
			<div class="container">
				<div class="text-sm-center">
					<form action="SD" method="post">
						<button class="btn btn-dark" type="submit" name="deconnection"
							value="deconnection">Deconnexion</button>

					</form>
					<br />
				</div>
			</div>
		</div>
	</div>
</body>
</html>