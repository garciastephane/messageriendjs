<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/styles.css">

</head>
<title>Modifier un utilisateur</title>
</head>
<body>
	<div class="container" style="max-width: 40% ">

		<div class="bg-white rounded ">
			
	<c:choose>
		<c:when test="${ empty sessionScope.authentification }">
			<c:redirect url="r" />
		</c:when>
	</c:choose>
	<div class="container">
		<br /> <br />
		<div class="text-sm-center">
		<h2>
			<strong>MODIFICATION D'UN UTILISATEUR</strong>
		</h2>
	</div>
	<br />
	<br />
	<br />
	<div class="container" id="containerChoixUser">
		<form class="needs-validation" action="SMU" method="post">
			<div class="text-sm-center">
			<input hidden="" name="id" value="${ id }">

			
				<div class="text-sm-center">
					<label>Nom</label> <input type="text" class="form-control"
						name="nom" value='<c:out value="${ personne.nom }"/>'>
				</div>
				<br>
				<div class="text-sm-center">
					<label>Prenom</label> <input type="text" class="form-control"
						name="prenom" value='<c:out value="${ personne.prenom }"/>'>
				</div>
				<br>

				<div class="text-sm-center">
					<label>Mail</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">@</span>
						</div>
						<input type="text" class="form-control" name="mail"
							value='<c:out value="${ personne.email }"/>'>
					</div>
				</div>
			</div>
			<br>

			<div class="text-sm-center">
				<div class="text-sm-center">
					<label>Adresse</label> <input type="text" class="form-control"
						name="adresse" value='<c:out value="${ personne.adresse }"/>'>
				</div>
				<br>

				<div class="text-sm-center">
					<label>Date de naissance</label> <input type="date"
						class="form-control" name="datenaissance"
						value='<c:out value="${ datenaissance }"/>'>
				</div>
				<br>

				<div class="text-sm-center">
					<label>Mot de Passe</label> <input type="password"
						class="form-control" name="password">
				</div>
				<br>

				<div class="text-sm-center">
					<label>Confirmer le Mot de Passe</label> <input type="password"
						class="form-control" name="password2">
				</div>
			</div>
			<br>
			<br>

			<div class="col text-center">
			<div>
				<button name="modif" class="btn btn-primary" value="valider">valider</button>
				<button name="modif" class="btn btn-secondary" value="activer / desactiver">activer / désactiver</button>
				<button name="modif" class="btn btn-primary" value="supprimer">supprimer</button>
			</div>
		</form>
		<br /> <br />
		<form action="SChU">
			<button class="btn btn-dark">Retour</button>
		</form>
	</div>
	<br>
	<br>
</body>
</html>