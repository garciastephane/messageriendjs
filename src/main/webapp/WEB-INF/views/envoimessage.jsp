<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/styles.css">

<title>Envoi message</title>
</head>
<body>
	
	<div class="container" id="containerMessage">

		<div class="bg-white rounded ">
			<content>
	<c:choose>
		<c:when test="${ empty sessionScope.authentification }">
			<c:redirect url="r" />
		</c:when>
	</c:choose>
	<div class="text-sm-center">
		<br /> <br />
		<h2>
			<strong>ENVOYER MESSAGE</strong>
		</h2>
	</div>
	<br />
	<br />
	<div class="container" id="containerMessage">
	<div class="text-sm-center">

		<form method="post" action="SEM">
			To : <br><input type="text" name="dest">
			<br>
			<br>
			Objet : <br><input type="text" name="obj" >
			<br>
			<br>
			<br>
			Ecrivez votre message : <br><textarea name="message" rows="10"></textarea>
			<br>
			<br>
			<input type="submit" id="submitMessage">
		</form>
		<br>
		<br>
		<br>
		<div class="col text-center">
		<form action="Retour" method="post">
					<button class="btn btn-dark" type="submit">Retour</button>
					<br> <br>
				</form>
		<br>
		<br>
	</div>
</content>
</body>
</html>