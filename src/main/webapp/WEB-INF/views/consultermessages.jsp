<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<title>Consultation des messages</title>
<meta charset="utf-8">
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/styles.css">
</head>

<body>
<div class="container">
<div class="bg-white rounded ">
			<div class="text-sm-center">
<h2><strong>LISTE DES MESSAGES</strong></h2>

<table class="table">
<thead class="table table-dark">
  <tr>
    <th>Expediteur</th>
    <br><br>
    <th>Objet</th>
   </tr>
 </thead>
  <tbody>
				<c:forEach var="coupleMessages" items="${listeMessages}">

					<tr>
						<td><c:out value="${coupleMessages ['expediteur'] ['nom'] } " /></td>
					<td><c:out value="${coupleMessages ['objet'] } " /></td>
					<td>
					<form action="SMC" method="get" >
						<input hidden="" id="m" value="${coupleMessages}">
						<button>voir</button>
					</form>
					
					</td>						
					</tr>
				</c:forEach>

			</tbody>
			
</table>
</div>
</div>
</div>

</body>
</html>