<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/styles.css">

<title>Choix de l'utilisateur</title>
</head>
<body>


	<div class="container">

		<div class="bg-white rounded ">
			<content> <c:choose>
				<c:when test="${ empty sessionScope.authentification }">
					<c:redirect url="r" />
				</c:when>
			</c:choose>
			<div class="text-sm-center">
				<br /> <br />
				<h2>
					<strong>CHOIX D'UN UTILISATEUR</strong>
				</h2>
			</div>
			<br />
			<br />
			<br />
			<div class="text-sm-center">
				<form class="needs-validation" action="SChU" method="post">
					<div class="text-sm-center">
						<div class="text-sm-center">
							<label>id utilisateur</label> <input class="form-control-sm"
								type="number" name="${ choix }"> <br /> <br />
							<button class="btn btn-primary" type="submit" value="Valider">Valider</button>
							<br /> <br />
						</div>
					</div>
				</form>
				<div class="row">
					<div class=col-md-12>
						<table class="table">

							<thead>
								<tr class="table table-dark">
									<td>Id</td>
									<td>Nom</td>
									<td>Prenom</td>
									<td>Date de naissance</td>
									<td>Mail</td>
									<td>Adresse</td>
								</tr>
							</thead>
							<tbody>
								<c:out value="${ alluser }" escapeXml="false"></c:out>

							</tbody>
						</table>
					</div>
				</div>
				<form action="Retour" method="post">
					<br /> <br />
					<button class="btn btn-dark">Retour</button>
				</form>
				<br /> <br />
			</div>
			</content>
</body>
</html>