<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/styles.css">

<title>Message</title>
</head>
<body>



	<div class="container">
		<div class="bg-white rounded ">
			<div class="text-sm-center">
				<br> <br>
				<h2>
					<strong>MESSAGE</strong>
				</h2>
			</div>
			<br> <br> <br>
			<div>
			<div class="text-sm">
				
				 <p>Expéditeur : ${ param.exp }<br>
				  Objet : ${ param.obj }</p>
			</div>
			<div class="div-primary">
				
			
					<div class="text-sm">
						<c:out value="${ param.cor }"></c:out>
						<br>
						<br>
					
				</div>
			</div>
</div>
			<div class="col text-center">
		<form action="Retour" method="post">
					<button class="btn btn-dark" type="submit">Retour</button>
					<br> <br>
				</form>
				</div>
			

		</div>
	</div>
</body>
</html>