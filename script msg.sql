CREATE OR REPLACE FUNCTION public.archivage()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$
	begin
		insert into archive(id_archive, nom, prenom, datenaissance, mail, adresse,id_type_profil) values (nextval('archive_seq'),old.nom, old.prenom, old. datenaissance, old.mail, old.adresse, old.id_type_profil);
		return old;
	end
$function$;

CREATE OR REPLACE function archivagemes() returns trigger AS $archive$
	begin
		insert into archivemessage(id_archivemes, corps, destinataires, expediteur, objet) values (nextval('archivemes_seq'),old.corps, old.corps, old.corps, old.objet);
		return old;
	end
$archive$ LANGUAGE plpgsql;

CREATE TRIGGER archivermessage before DELETE ON message FOR EACH ROW EXECUTE FUNCTION archivagemes();
CREATE TRIGGER archiver before DELETE ON profil FOR EACH ROW EXECUTE FUNCTION archivage();

INSERT INTO typeprofil(id_type_profil, libelle)VALUES(1, 'Administrateur');
INSERT INTO typeprofil(id_type_profil, libelle)VALUES(2, 'Utilisateur');
INSERT INTO profil(id_profil, nom, prenom, datenaissance, actif, adresse, mail, id_type_profil) VALUES(1, 'Gali', 'Seti', '2020-01-17', true, 'sg@s.g', 'labas', 1);
INSERT INTO profil(id_profil, nom, prenom, datenaissance, actif, adresse, mail, id_type_profil) VALUES(2, 'a', 'a', '2020-01-30', true, 'a@a.a', 'ici', 1);
INSERT INTO profil(id_profil, nom, prenom, datenaissance, actif, adresse, mail, id_type_profil) VALUES(3, 'b', 'b', '2020-01-03', true, 'b@b.b', 'la', 2);
INSERT INTO login(id_profil, login, motdepasse)VALUES(1, 'admin', 'admin');
INSERT INTO login(id_profil, login, motdepasse)VALUES(2, 'a', 'a');
INSERT INTO login(id_profil, login, motdepasse)VALUES(3, 'b', 'b');